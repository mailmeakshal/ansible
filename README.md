# Ansible
# In Master instance 
1. Install the ansible
```
sudo yum install ansible -y
sudo yum update
```
2. Add ansible.cfg,hosts,my-playbook.yaml
3. Create and update the pem file
```
sudo nano client.pem
```
4. Giving read and ownership for ec2-user
```
sudo chmod 400 client.pem
sudo chown ec2-user client.pem
```
5. Executing the ansible by 
```
ansible-playbook my-playbook.yaml -vv
```
